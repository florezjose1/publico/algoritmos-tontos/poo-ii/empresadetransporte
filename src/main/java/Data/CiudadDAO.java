/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import Model.Ciudad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author flore
 */
public class CiudadDAO {
    private static final String SQL_GET_ALL = "SELECT nombre, codigo, departamento FROM ciudad";
    private static final String SQL_CREAR = "INSERT INTO `ciudad`(`nombre`, `codigo`, `departamento`) VALUES (?, ?, ?)";
    private static final String SQL_UPDATE = "UPDATE `ciudad` SET `nombre`=?, `departamento`=? WHERE codigo=?";
    private static final String SQL_DELETE = "DELETE `ciudad` WHERE codigo=?";

    public List<Ciudad> consultar() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Ciudad ciudad;
        List<Ciudad> ciudades = new ArrayList<>();

        try {
            con = ConnectionDB.getConnection();
            ps = con.prepareStatement(this.SQL_GET_ALL);
            res = ps.executeQuery();
            while (res.next()) {
                ciudad = new Ciudad(
                        res.getString("nombre"),
                        res.getString("codigo"),
                        res.getString("departamento")
                );

                ciudades.add(ciudad);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CiudadDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConnectionDB.close(res);
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return ciudades;
    }

    public int crear(Ciudad c) {
        return this.ejecutarSQL(c, 3);
    }
    
    public int actualizar(Ciudad c) {
        return this.ejecutarSQL(c, 2);
    }
    
    public int eliminar(Ciudad c) {
        return this.ejecutarSQL(c, 1);
    }
    
    private int ejecutarSQL(Ciudad c, int t) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConnectionDB.getConnection();
            switch (t) {
                case 1: {
                    ps = con.prepareStatement(this.SQL_DELETE);
                    ps.setString(1, c.getCodigo());
                    break;
                }
                case 2: {
                    ps = con.prepareStatement(this.SQL_UPDATE);
                    ps.setString(1, c.getNombre());
                    ps.setString(2, c.getDepartamento());
                    ps.setString(3, c.getCodigo());
                    break;
                }
                case 3: {
                    ps = con.prepareStatement(this.SQL_CREAR);
                    ps.setString(1, c.getNombre());
                    ps.setString(2, c.getCodigo());
                    ps.setString(3, c.getDepartamento());

                    break;
                }
                default: break;
            }
            
            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(CiudadDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(CiudadDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return registros;
    }
}
