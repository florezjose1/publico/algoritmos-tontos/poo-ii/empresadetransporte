/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import Model.EmpresaDTO;
import java.util.List;

/**
 *
 * @author flore
 */
public interface EmpresaDAO {
    
    public List<EmpresaDTO> consultar();
    public int crear(EmpresaDTO e);
    public int actualizar(EmpresaDTO e);
    public int eliminar(EmpresaDTO e);
    
}
