/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import Model.EmpresaDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author flore
 */
public class EmpresaDAOJDBC implements EmpresaDAO {

    private static final String SQL_ALL = "SELECT nombre, nit, correo, clave FROM empresa";
    private static final String SQL_CREAR = "INSERT INTO `empresa`(`nombre`, `nit`, `correo`, `clave`) VALUES (?, ?, ?, ?)";
    private static final String SQL_UPDATE = "UPDATE `empresa` SET `nombre`=?, `correo`=?,`clave`=? WHERE nit=?";
    private static final String SQL_DELETE = "DELETE FROM `empresa` WHERE nit=?";

    @Override
    public List<EmpresaDTO> consultar() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        EmpresaDTO empresa;
        List<EmpresaDTO> empresas = new ArrayList<>();

        try {
            con = ConnectionDB.getConnection();
            ps = con.prepareStatement(this.SQL_ALL);
            res = ps.executeQuery();
            while (res.next()) {
                empresa = new EmpresaDTO(
                        res.getString("nombre"),
                        res.getString("nit"),
                        res.getString("correo"),
                        res.getString("clave")
                );

                empresas.add(empresa);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmpresaDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConnectionDB.close(res);
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(EmpresaDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return empresas;
    }

    @Override
    public int crear(EmpresaDTO e) {
        return this.ejecutarSQL(e, 3);
    }

    @Override
    public int actualizar(EmpresaDTO e) {
        return this.ejecutarSQL(e, 2);
    }

    @Override
    public int eliminar(EmpresaDTO e) {
        return this.ejecutarSQL(e, 1);
    }

    private int ejecutarSQL(EmpresaDTO e, int t) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConnectionDB.getConnection();
            switch (t) {
                case 1: {
                    ps = con.prepareStatement(this.SQL_DELETE);
                    ps.setString(1, e.getNit());
                    break;
                }
                case 2: {
                    ps = con.prepareStatement(this.SQL_UPDATE);
                    ps.setString(1, e.getNombre());
                    ps.setString(2, e.getCorreo());
                    ps.setString(3, e.getClave());
                    ps.setString(4, e.getNit());
                    break;
                }
                case 3: {
                    ps = con.prepareStatement(this.SQL_CREAR);
                    ps.setString(1, e.getNombre());
                    ps.setString(2, e.getNit());
                    ps.setString(3, e.getCorreo());
                    ps.setString(4, e.getClave());
                    break;
                }
                default: break;
            }
            
            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(EmpresaDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConnectionDB.close(ps);
                ConnectionDB.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(EmpresaDAOJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return registros;
    }
}
