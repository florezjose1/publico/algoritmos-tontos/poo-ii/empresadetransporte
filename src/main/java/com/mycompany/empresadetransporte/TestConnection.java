/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.empresadetransporte;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author flore
 */
public class TestConnection {
    public static void main(String[] args) {
        String db = "empresadetransporte";
        String username = "root";
        String password = "";
        String url = "jdbc:mysql://localhost:3306/"+db+"?useSSL=false&useTimezone=true&serverTimezone=UTC&allowPublicKeyRetrieval=true";
        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            Statement sentence = connection.createStatement();
            String sql = "SELECT nombre, nit, correo, clave FROM empresa";
            ResultSet result = sentence.executeQuery(sql);
            while(result.next()) {
                System.out.println("Nombre: " +  result.getString("nombre"));
            }
            result.close();
            sentence.close();
            connection.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(TestConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
