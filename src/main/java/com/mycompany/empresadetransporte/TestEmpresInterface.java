/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.empresadetransporte;

import Data.EmpresaDAO;
import Data.EmpresaDAOJPA;
import Model.EmpresaDTO;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author flore
 */
public class TestEmpresInterface {
    public static void main(String[] args) {
        EmpresaDAO e = new EmpresaDAOJPA();
        List<EmpresaDTO> empresas = new ArrayList<>();
        empresas = e.consultar();
        for(EmpresaDTO emp : empresas) {
            System.out.println(emp.toString());
        }
        
        // Insertar
        EmpresaDTO n = new EmpresaDTO("Atilan", "1222", "atilan@gmail.com", "Clave123");
        e.crear(n);
        
        // Actualizar
        n.setNombre("Atilan 1");
        e.actualizar(n);
        
        // Eliminar
        // e.eliminar(n);
    }
}
